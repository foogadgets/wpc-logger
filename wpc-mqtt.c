#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <MQTTClient.h>
#include "wpc-mqtt.h"
#include "configuration.h"


extern MQTTClient client;
extern MQTTClient_connectOptions conn_opts;
extern MQTTClient_message pubmsg;
extern char value[32];
extern char topic[64];


int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
    int i;
    char* payloadptr;
//    printf("Message arrived\n");
//    printf("     topic: %s\n", topicName);
//    printf("   message: ");
    payloadptr = message->payload;
    for(i=0; i<message->payloadlen; i++)
    {
        putchar(*payloadptr++);
    }
    putchar('\n');
    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}

void connlost(void *context, char *cause)
{
    printf("\nConnection lost\n");
    printf("     cause: %s\n", cause);
}

int pub_to_mqtt(WPC* _my_wpc)
{
    int rc;
    if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
    {
        printf("Failed to connect, return code %d\n", rc);
        exit(EXIT_FAILURE);
    }
    pubmsg.qos = QOS;
    pubmsg.retained = 0;

    sprintf(topic, "%s%s", TOPIC, "power");
    sprintf(value, "%d", _my_wpc->powerW);
    pubmsg.payload = value;
    pubmsg.payloadlen = strlen(value);
    MQTTClient_publishMessage(client, topic, &pubmsg, NULL);

    sprintf(topic, "%s%s", TOPIC, "energy");
    sprintf(value, "%.1f", _my_wpc->totEkWh);
    pubmsg.payload = value;
    pubmsg.payloadlen = strlen(value);
    MQTTClient_publishMessage(client, topic, &pubmsg, NULL);

    //printf("Waiting for publication of %s on topic %s for client with ClientID: %s\n", value, topic, CLIENTID);
    MQTTClient_disconnect(client, TIMEOUT);

    return rc;
}


int wpc_init_mqtt(const char* mqttAddress)
{
    conn_opts = (MQTTClient_connectOptions) MQTTClient_connectOptions_initializer;
    pubmsg = (MQTTClient_message) MQTTClient_message_initializer;
    MQTTClient_create(&client, mqttAddress, CLIENTID,
        MQTTCLIENT_PERSISTENCE_NONE, NULL);
    conn_opts.keepAliveInterval = 10;
    conn_opts.cleansession = 1;
    MQTTClient_setCallbacks(client, NULL, connlost, msgarrvd, NULL);
    memset(value, '\0', 32);
    memset(topic, '\0', 64);
    sprintf(topic, "%s", TOPIC);

    return 0;
}
