#include <mariadb/mysql.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "wpc-sql.h"
#include "configuration.h"


/*
CREATE TABLE 'wpc' (
  'wpc_key'	BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  'id'		SMALLINT UNSIGNED DEFAULT NULL,
  'date'	TIMESTAMP,
  'power'	INT UNSIGNED DEFAULT NULL,
  'energy'	BIGINT UNSIGNED DEFAULT NULL
);
*/


MYSQL *conn;

void finish_with_error(MYSQL *con) {
	fprintf(stderr, "%s\n", mysql_error(con));
	mysql_close(con);
	exit(1);        
}


void wpc_init_sql(WPC* _my_wpc) {
	char statement[512];
	conn = mysql_init(NULL);
	
	if (conn == NULL) {
		fprintf(stderr, "%s\n", mysql_error(conn));
		exit(1);
	}  

	if (mysql_real_connect(conn, SQL_SERVER, SQL_USER, SQL_PASS,
				NULL, 0, NULL, 0) == NULL) {
		finish_with_error(conn);
	}
	
	snprintf(statement, 512, "CREATE DATABASE IF NOT EXISTS %s", "WPC");
	if (mysql_query(conn, statement)) {
		finish_with_error(conn);
	} else {
		snprintf(statement, 512, "USE %s", "WPC");
		if (mysql_query(conn, statement)) {
			finish_with_error(conn);
		}
		snprintf(statement, 512, "CREATE TABLE IF NOT EXISTS %s (wpc_key BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY, id SMALLINT UNSIGNED DEFAULT NULL, date TIMESTAMP, power INT UNSIGNED DEFAULT NULL, energy BIGINT UNSIGNED DEFAULT NULL)", SQL_TABLE);
		if (mysql_query(conn, statement)) {
			finish_with_error(conn);
		}
	} 
}


void log_to_sql(WPC* _my_wpc) {
	char statement[128];
	char _date[20];
	time_t _ts = _my_wpc->oldTimestamp + _my_wpc->deltaT;

	strftime(_date, 20, "%Y-%m-%d %H:%M:%S", localtime(&_ts));

	snprintf(statement, 512, "INSERT INTO %s (id, date, power, energy) VALUES (%i, '%s', %i, %.0f)", SQL_TABLE, WPC_ID, _date, _my_wpc->powerW, _my_wpc->totEkWh);

	if (mysql_query(conn, statement)) {
		finish_with_error(conn);
	}
}


void wpc_close_sql() {
	mysql_close(conn);
	exit(0);
}

