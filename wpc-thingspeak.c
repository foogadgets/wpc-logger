#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include "wpc-thingspeak.h"
#include "configuration.h"


void log_to_thingspeak(WPC *_wpc) {
	char* str_url;
	char* str_post;
	size_t string_size_needed;

        CURL *curl;
        CURLcode res;


	string_size_needed = snprintf(NULL, 0, "timezone=Europe/Stockholm&headers=false&key=%s&field1=%.3f&field2=%.3f&field3=%.3f", THINGSPEAK_API_KEY, _wpc->powerW/1000.0, _wpc->energyWh/1000.0, _wpc->energyWh/1000.0);
	str_post = malloc((string_size_needed+1)*sizeof(char));
	snprintf(str_post, string_size_needed+1, "timezone=Europe/Stockholm&headers=false&key=%s&field1=%.3f&field2=%.3f&field3=%.3f", THINGSPEAK_API_KEY, _wpc->powerW/1000.0, _wpc->energyWh/1000.0, _wpc->energyWh/1000.0);

	string_size_needed = snprintf(NULL, 0, "http://api.thingspeak.com/update");
	str_url = malloc((string_size_needed+1)*sizeof(char));
	snprintf(str_url, string_size_needed+1, "http://api.thingspeak.com/update");

	curl = curl_easy_init();

	if(curl) {

		curl_easy_setopt(curl, CURLOPT_URL, str_url);
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST"); /* !!! */

		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, str_post); /* data goes here */

		curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
	
		res = curl_easy_perform(curl);

		/* Check for errors */ 
		if(res != CURLE_OK)
			fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));

		curl_easy_cleanup(curl);

	}
	free(str_url);
	free(str_post);
}

