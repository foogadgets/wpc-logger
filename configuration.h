#ifndef WPC_CONFIGURATION_H_   /* Include guard */
#define WPC_CONFIGURATION_H_

// Typical values are 800, 1000 or 10000 impulses/kWh
#define BLINK_PER_KWH	1000

// Uncomment and insert XIVELY Data if you want to log to the http://xively.com service.
#define XIVELY_API_KEY	"W1gJux0Avf7LrofSz82a13qVbESKey9uRp874TNy8BUNPD2B"
#define XIVELY_FEED_ID	"940167157"

// Uncomment and insert EMON Data if you want to log to the http://emoncms.org service.
#define	EMON_API_KEY	"24ad2b25c5fb1cc004a3ee1552979246"

// Uncomment and insert open sense Data if you want to log to the http://open.sen.se service.
#define OSENSE_POWER_FEED_ID	"58731"
#define OSENSE_ENERGY_FEED_ID	"58732"
#define OSENSE_API_KEY		"w7N1rArkthXRe8gqDpdjww"

// Uncomment and insert Thingspeak Data if you want to log to the http://thingspeak.com service.
#define	THINGSPEAK_API_KEY "85MM3OO4Y7Q6DX3U"


// Uncomment and insert MySQL data if you want to log to a MySQL server
// Create a MySQL user with rights to create a wpc-table,
// mysql> CREATE USER wpc@localhost IDENTIFIED BY 'wpc';
// mysql> GRANT ALL ON WPC.* to wpc@localhost;
#define SQL_SERVER	"localhost"
#define SQL_USER	"wpc"
#define SQL_PASS	"wpc"
#define SQL_TABLE	"wpc"

// Uncomment this if you want random data to be produced.
// This overrides the readings from the WPC.
//#define TEST_LOGGING

#define WPC_ID	    	70
#define WPC_TYPE        "temperaturehumidity"

#define MQTTADDR    "192.168.1.110:1883"
#define CLIENTID    "wpc-logger"
#define QOS         0
#define TOPIC       "Pukslagargatan/sensors/wpc/"
#define TIMEOUT     10000L

#endif

