#include <stdio.h>
#include <stdlib.h>
#include "../common.h" // WPC struct


int skip_hi_loop = 1;
int skip_lo_loop = 1;
int temp_lo_count = 0;
int temp_hi_count = 0;
extern float f_blinkFactor;

int gen_random_data(WPC *_wpc) {

	if (skip_hi_loop==0) { // Generate the big numbers
		skip_hi_loop = rand()%20;
		temp_hi_count = (int)((rand()%120+1)/f_blinkFactor);
	} else {
		skip_hi_loop -= 1;
	}

	if (skip_lo_loop==0) { // Generates the smaller rippel
		skip_lo_loop = rand()%5;
		temp_lo_count = (int)((rand()%12+1)/f_blinkFactor);
	} else {
		skip_lo_loop -= 1;
	}

	_wpc->deltaT = 60;

	return (_wpc->prevCounts + temp_hi_count + temp_lo_count);

}

