#ifndef _WPC_MQTT_H_
#define _WPC_MQTT_H_

#include <MQTTClient.h>
#include "common.h"


MQTTClient client;
MQTTClient_connectOptions conn_opts;
MQTTClient_message pubmsg;
char value[32];
char topic[64];
 
int wpc_init_mqtt(const char*);
int pub_to_mqtt(WPC*);


#endif /* _WPC_MQTT_H_ */
