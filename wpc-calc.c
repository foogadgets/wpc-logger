#include <stdio.h>
#include <stdlib.h>
#include <math.h> // signbit
#include "wpc-calc.h"
#include "configuration.h"


float f_blinkFactor = 1000/BLINK_PER_KWH;

void calculate_power_and_energy(WPC *_wpc) {
	long i_diff = 1;
	long i_counts = 0;

	if ( _wpc->temperature >= 0.0 ) {
		i_counts = _wpc->humidity*4096 + (int)(10*_wpc->temperature);
	} else {
		i_counts = _wpc->humidity*4096 - (int)(10*_wpc->temperature) + 2048;
	}

	i_diff = i_counts - _wpc->prevCounts;
	_wpc->prevCounts = i_counts;

	if ( i_diff < 0 ) {	// Check if a counter overflow occured
		i_diff += 393216;
	}

	_wpc->powerW = (int)(i_diff*f_blinkFactor*60.0*(60.0/_wpc->deltaT));
	_wpc->energyWh = (int)(i_diff*f_blinkFactor);
	_wpc->totEkWh += _wpc->energyWh/1000.0;
}

