#!/bin/bash

### BEGIN INIT INFO
# Provides:          wpcd
# Required-Start:    $all
# Required-Stop:     $local_fs $remote_fs
# Should-Start:
# Should-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: A simple logging application for the Wireless Pulse Counter from foogadgets
# Description:       It can log to http://xively.com, http://emoncms.org, http://thingspeak.com,
#			http://open.sen.se and to a MySQL-server.
### END INIT INFO

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

DAEMON=/usr/local/bin/wpcd
NAME=wpcd
DESC="Wireless Pulse Counter logger daemon"
PIDFILE=/var/run/$NAME.pid
SCRIPTNAME=/etc/init.d/$NAME
LOGDIR=/var/log
LOGFILE=$LOGDIR/$NAME.log
DAEMON_OPTS=""

test -x $DAEMON || exit 0

. /lib/lsb/init-functions

# Include defaults if available
if [ -f /etc/$NAME.conf ] ; then
    . /etc/$NAME.conf
fi

case "$1" in
  start)
    echo "Starting wpcd daemon"
    /sbin/start-stop-daemon --start --pidfile $PIDFILE \
        --user www-data --group www-data \
        -b --make-pidfile \
        --exec $DAEMON $DAEMON_OPTS
    ;;
  stop)
    echo "Stopping wpcd daemon"
    /sbin/start-stop-daemon --stop --pidfile $PIDFILE --verbose
    ;;
  *)
    echo "Usage: /etc/init.d/wpcd {start|stop}"
    exit 1
    ;;
esac

exit 0

