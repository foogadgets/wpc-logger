#include <stdio.h> // printf
#include <stdlib.h> // srand, atof
#include <unistd.h> // sleep
#include <string.h> // strcmp
#include "wpc-calc.h"
#include "wpc-telldus.h"
#include "configuration.h"

//#include "wpc-sql.h"
#include "wpc-mqtt.h"
#include "wpc-emon.h"
#include "wpc-xively.h"
#include "wpc-opensense.h"
#include "wpc-thingspeak.h"


#define RELAX_TIME 7	// Seconds to sleep between occured sensor events.

int main(int argc, char *argv[]) {
	int callbackId;
	time_t initTime = time(NULL);
	srand(initTime);

	WPC my_wpc = {"WPC", 0.0, 0, 1, initTime, 0, 0, 0, 0.0, 0};

	if ( (argc==2) || (argc>3) ) { /* argc should be 1 or 3 for correct execution */
		printf( "Usage: %s [-e <start_energy in kWh>]\n\n", argv[0] );
		return 0;
	} else {
		if ( (argc == 3) && (strcmp(argv[1],"-e")==0) ) {
			my_wpc.totEkWh = atof(argv[2]);
		}
	}

//	wpc_init_sql(&my_wpc);
        wpc_init_mqtt(MQTTADDR);

	tdInit();
	callbackId = tdRegisterSensorEvent( (TDSensorEvent)&sensorEvent, &my_wpc );

        while(1) {
                sleep(RELAX_TIME);

                if (my_wpc.newDataAvailable) { // Only log if there have occured an event while sleeping RELAX_TIME seconds.
                        calculate_power_and_energy(&my_wpc);

//			log_to_sql(&my_wpc);
                        pub_to_mqtt(&my_wpc);
//			log_to_xively(&my_wpc);
//			log_to_emoncms(&my_wpc);
//			log_to_opensense(&my_wpc);
//			log_to_thingspeak(&my_wpc);
			my_wpc.newDataAvailable = 0;
		}
	}
        /* Cleanup */
        tdUnregisterCallback( callbackId );
        tdClose();
//	wpc_close_sql();

        return 0;
}

