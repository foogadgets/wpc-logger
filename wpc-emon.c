#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include "wpc-emon.h"
#include "configuration.h"



void log_to_emoncms(WPC *_wpc) {
	char* str_emon;
	size_t string_size_needed;

	CURL *curl;
	CURLcode res;

	string_size_needed = snprintf(NULL, 0, "http://emoncms.org/input/post?apikey=%s&json={power:%i}", EMON_API_KEY, _wpc->powerW);
	str_emon = malloc((string_size_needed+1)*sizeof(char));
	snprintf(str_emon, string_size_needed+1, "http://emoncms.org/input/post?apikey=%s&json={power:%i}", EMON_API_KEY, _wpc->powerW);

	curl = curl_easy_init();

	if(curl) {
		curl_easy_setopt(curl, CURLOPT_URL, str_emon);
//		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 0L);
 
		res = curl_easy_perform(curl);
		/* Check for errors */ 
		if(res != CURLE_OK)
			fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));

		curl_easy_cleanup(curl);
	}
	free(str_emon);
}

