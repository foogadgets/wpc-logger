#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include "wpc-opensense.h"
#include "configuration.h"


void log_to_opensense(WPC *_wpc) {
	char* str_power;
	char* str_energy;
	char* str_json;
	char str_url[26];
	char* str_header;
	size_t string_size_needed;
	struct curl_slist* slist = NULL;

        CURL *curl;
        CURLcode res;

	string_size_needed = snprintf(NULL, 0, "{\"feed_id\": \"%s\",\"value\": \"%d\"}", OSENSE_POWER_FEED_ID, _wpc->powerW);
	str_power = malloc((string_size_needed+1)*sizeof(char));
	snprintf(str_power, string_size_needed+1, "{\"feed_id\": \"%s\",\"value\": \"%d\"}", OSENSE_POWER_FEED_ID, _wpc->powerW);

	string_size_needed = snprintf(NULL, 0, "{\"feed_id\": \"%s\",\"value\": \"%.3f\"}", OSENSE_ENERGY_FEED_ID, _wpc->energyWh/1000.0);
	str_energy = malloc((string_size_needed+1)*sizeof(char));
	snprintf(str_energy, string_size_needed+1, "{\"feed_id\": \"%s\",\"value\": \"%.3f\"}", OSENSE_ENERGY_FEED_ID, _wpc->energyWh/1000.0);

	string_size_needed = snprintf(NULL, 0, "[%s,%s]", str_power, str_energy);
	str_json = malloc((string_size_needed+1)*sizeof(char));
	snprintf(str_json, string_size_needed+1, "[%s,%s]", str_power, str_energy);

	string_size_needed = snprintf(NULL, 0, "sense_key:%s", OSENSE_API_KEY);
	str_header = malloc((string_size_needed+1)*sizeof(char));
	snprintf(str_header, string_size_needed+1, "sense_key:%s", OSENSE_API_KEY);

	strcpy(str_url, "http://api.sen.se/events/");


	curl = curl_easy_init();

	if(curl) {

		slist = curl_slist_append(slist, str_header);

		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
		curl_easy_setopt(curl, CURLOPT_URL, str_url);
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST"); /* !!! */

		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, str_json); /* data goes here */

		curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
	
		res = curl_easy_perform(curl);

		/* Check for errors */ 
		if(res != CURLE_OK)
			fprintf(stderr, "curl_easy_perform() failed: %s\n\n", curl_easy_strerror(res));

		curl_slist_free_all(slist);
		curl_easy_cleanup(curl);

	}
	free(str_power);
	free(str_energy);
	free(str_json);
	free(str_header);
}

