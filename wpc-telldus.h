#ifndef WPC_WPC_TELLDUS_H_   /* Include guard */
#define WPC_WPC_TELLDUS_H_
#include <telldus-core.h>

void WINAPI sensorEvent(const char *, const char *, int, int, const char *, int, int, void *);

#endif
