# README #

This is a simple logger application for the [Wireless Pulse Counter](http://foogadgets.tictail.com/product/wireless-pulse-counter) from [foogadgets](http://foogadgets.tictail.com)

The repository contains an example of a WPC logger application for the Tellstick DUO written in C.

There are several possible ways of logging. MySQL-server or Cloud services.

The Cloud services are [Xively](http://xively.com),  [Open.Sen.se](http://open.sen.se), [ThingSpeak](http://thingspeak.com) and [Emoncms](http://emoncms.org). If you'd like to try them out you can just create a user account. After you have registered you will receive the needed information to insert into the configuration.h-file;
Here are some examples, [open.sen.se](http://open.sen.se/sensemeters/tab/7160/?preview=on)
, [xively](https://xively.com/feeds/940167157) and [ThingSpeak](https://thingspeak.com/channels/19664)

```
#!c

// Uncomment and insert XIVELY Data if you want to log to the http://xively.com service.
#define XIVELY_API_KEY	""
#define XIVELY_FEED_ID	""

// Uncomment and insert EMON Data if you want to log to the http://emoncms.org service.
#define	EMON_API_KEY	""

// Uncomment and insert OpenSenseData if you want to log to the http://open.sen.se service.
#define OSENSE_POWER_FEED_ID	""
#define OSENSE_ENERGY_FEED_ID	""
#define OSENSE_API_KEY		""

// Uncomment and insert Thingspeak Data if you want to log to the http://thingspeak.com service.
#define THINGSPEAK_API_KEY      ""


// Uncomment and insert MySQL data if you want to log to a MySQL server
// Create a MySQL user with rights to create a wpc-table,
// mysql> CREATE USER wpc@localhost IDENTIFIED BY 'wpc';
// mysql> GRANT ALL ON WPC.* to wpc@localhost;
#define SQL_SERVER	"localhost"
#define SQL_USER	"wpc"
#define SQL_PASS	"wpc"
#define SQL_TABLE	"wpc"

```



If SQL is enabled it will create the Database "WPC" and a Table inside that database called "wpc". The table has the following format;

```
#!c

+---------+----------------------+------+-----+-------------------+-----------------------------+
| Field   | Type                 | Null | Key | Default           | Extra                       |
+---------+----------------------+------+-----+-------------------+-----------------------------+
| wpc_key | bigint(20) unsigned  | NO   | PRI | NULL              | auto_increment              |
| id      | smallint(5) unsigned | YES  |     | NULL              |                             |
| date    | timestamp            | NO   |     | CURRENT_TIMESTAMP | on update CURRENT_TIMESTAMP |
| power   | int(10) unsigned     | YES  |     | NULL              |                             |
| energy  | bigint(20) unsigned  | YES  |     | NULL              |                             |
+---------+----------------------+------+-----+-------------------+-----------------------------+

```

The "power"-field stores the current average power usage during the last minute in [W].
The "energy"-field stores the accumulated energy consumption from the start of this application in [kWh].


# INSTALL #
You need the development packages for telldus-core, curl and mysql.
telldus-development files can be found [here](http://download.telldus.se/TellStick/Software/telldus-core/).


```
#!
$> sudo apt-get install libmysqlclient-dev libcurl4-gnutls-dev

```


The wpcd can be started with an offset Energy usage if you'd like the output to be in sync with your electric meter. If the Electric meter read 12345 kWh, then you start the wpcd with,

```
#!
$> ./wpcd -e 12345
```