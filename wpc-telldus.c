#include <stdlib.h> // atoi, atof
#include <string.h> // strcmp
#include "wpc-telldus.h"
#include "common.h"

void WINAPI sensorEvent(const char *protocol, const char *model, int sensorId, int dataType, const char *value, int ts, int callbackId, void *context) {
	if ( (sensorId == WPC_ID) && (strcmp(model,WPC_TYPE) == 0) ) {
		if (dataType == TELLSTICK_TEMPERATURE) {
			((WPC*)context)->temperature = atof(value);
		} else if (dataType == TELLSTICK_HUMIDITY) {
			((WPC*)context)->humidity = atoi(value);

			((WPC*)context)->deltaT = ts - ((WPC*)context)->oldTimestamp;
			((WPC*)context)->oldTimestamp = ts;

			((WPC*)context)->newDataAvailable = 1;

		} else {
			return;
		}
		return;
	}
}

