#ifndef WPC_COMMON_H_   /* Include guard */
#define WPC_COMMON_H_
#include <time.h>
#include "configuration.h"


// Global
typedef struct wpc WPC;

struct wpc {
	char	name[32];
	float	temperature;
	int	humidity;
	int	deltaT;
	time_t	oldTimestamp;
	int	prevCounts;
	int	energyWh;
	int	powerW;
	float	totEkWh;
	int	newDataAvailable;
};

#endif

