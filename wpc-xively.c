#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include "wpc-xively.h"
#include "configuration.h"


void log_to_xively(WPC *_wpc) {
	char* str_power;
	char* str_energy;
	char* str_json;
	char* str_url;
	char* str_header;
	size_t string_size_needed;
	struct curl_slist* slist = NULL;

        CURL *curl;
        CURLcode res;

	string_size_needed = snprintf(NULL, 0, "\"id\":\"%s_Power\", \"current_value\":\"%.3f\", \"tags\":\"foogadgets,power\", \"unit\": { \"symbol\":\"kW\", \"label\":\"Watts\", \"type\":\"basicSI\"}", _wpc->name, _wpc->powerW/1000.0);
	str_power = malloc((string_size_needed+1)*sizeof(char));
	snprintf(str_power, string_size_needed+1, "\"id\":\"%s_Power\", \"current_value\":\"%.3f\", \"tags\":\"foogadgets,power\", \"unit\": { \"symbol\":\"kW\", \"label\":\"Watts\", \"type\":\"basicSI\"}", _wpc->name, _wpc->powerW/1000.0);

	string_size_needed = snprintf(NULL, 0, "\"id\":\"%s_Energy\", \"current_value\":\"%.0f\", \"tags\":\"foogadgets,energy\", \"unit\": { \"symbol\":\"kWh\", \"label\":\"WattHours\", \"type\":\"basicSI\"}", _wpc->name, _wpc->totEkWh);
	str_energy = malloc((string_size_needed+1)*sizeof(char));
	snprintf(str_energy, string_size_needed+1, "\"id\":\"%s_Energy\", \"current_value\":\"%.0f\", \"tags\":\"foogadgets,energy\", \"unit\": { \"symbol\":\"kWh\", \"label\":\"WattHours\", \"type\":\"basicSI\"}", _wpc->name, _wpc->totEkWh);
	
	string_size_needed = snprintf(NULL, 0, "{\"version\":\"1.0.0\", \"datastreams\":[{%s}, {%s}]}", str_power, str_energy);
	str_json = malloc((string_size_needed+1)*sizeof(char));
	snprintf(str_json, string_size_needed+1, "{\"version\":\"1.0.0\", \"datastreams\":[{%s}, {%s}]}", str_power, str_energy);
	
	string_size_needed = snprintf(NULL, 0, "https://api.xively.com/v2/feeds/%s", XIVELY_FEED_ID);
	str_url = malloc((string_size_needed+1)*sizeof(char));
	snprintf(str_url, string_size_needed+1, "https://api.xively.com/v2/feeds/%s", XIVELY_FEED_ID);
	
	string_size_needed = snprintf(NULL, 0, "X-ApiKey: %s", XIVELY_API_KEY);
	str_header = malloc((string_size_needed+1)*sizeof(char));
	snprintf(str_header, string_size_needed+1, "X-ApiKey: %s", XIVELY_API_KEY);

	curl = curl_easy_init();

	if(curl) {

		slist = curl_slist_append(slist, str_header);

		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
		curl_easy_setopt(curl, CURLOPT_URL, str_url);
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT"); /* !!! */

		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, str_json); /* data goes here */

		curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
	
		res = curl_easy_perform(curl);

		/* Check for errors */ 
		if(res != CURLE_OK)
			fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));

		curl_slist_free_all(slist);
		curl_easy_cleanup(curl);

	}
	free(str_power);
	free(str_energy);
	free(str_json);
	free(str_url);
	free(str_header);
}

