CC=gcc
CFLAGS=-Wall -ltelldus-core -lcurl `mariadb_config --cflags --libs`
OBJS = main.o wpc-calc.o wpc-telldus.o wpc-mqtt.o wpc-sql.o wpc-emon.o wpc-xively.o wpc-opensense.o wpc-thingspeak.o test/gen_wpc_test_data.o
USR_CFLAGS += -lpaho-mqtt3cs


all: ${OBJS}
	${CC} -o wpcd ${OBJS} ${CFLAGS} ${USR_CFLAGS}

main.c:
	${CC} ${CFLAGS} ${USR_CFLAGS} -c main.c test/gen_wpc_test_data.c

wpc-calc.o: wpc-calc.c
	${CC} ${CFLAGS} ${USR_CFLAGS} -c wpc-calc.c
wpc-telldus.o: wpc-telldus.c
	${CC} ${CFLAGS} ${USR_CFLAGS} -c wpc-telldus.c
wpc-mqtt.o: wpc-mqtt.c
	${CC} ${CFLAGS} ${USR_CFLAGS} -c wpc-mqtt.c
wpc-sql.o: wpc-sql.c
	${CC} ${CFLAGS} ${USR_CFLAGS} -c wpc-sql.c
wpc-emon.o: wpc-emon.c
	${CC} ${CFLAGS} ${USR_CFLAGS} -c wpc-emon.c
wpc-xively.o: wpc-xively.c
	${CC} ${CFLAGS} ${USR_CFLAGS} -c wpc-xively.c
wpc-opensense.o: wpc-opensense.c
	${CC} ${CFLAGS} ${USR_CFLAGS} -c wpc-opensense.c
wpc-thingspeak.o: wpc-thingspeak.c
	${CC} ${CFLAGS} ${USR_CFLAGS} -c wpc-thingspeak.c

clean:
	rm -f ${OBJS} wpcd

install: all
	systemctl stop wpcd || true
	/bin/sleep 1
	systemctl disable wpcd || true
	cp wpcd /usr/local/bin/
	cp wpcd.service /usr/local/lib/systemd/system/
	systemctl daemon-reload
	systemctl enable wpcd
	systemctl start wpcd

uninstall:
	systemctl stop wpcd
	/bin/sleep 1
	systemctl disable wpcd
	rm -rf /usr/local/bin/wpcd
	rm -rf /usr/local/lib/systemd/system/wpcd.service
	systemctl daemon-reload
