#ifndef WPC_SQL_H_   /* Include guard */
#define WPC_SQL_H_
#include "common.h"

void wpc_init_sql(WPC*);
void log_to_sql(WPC*);
void wpc_close_sql();

#endif
